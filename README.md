# scope
Repository for shared identifier scope definitions for use with dv resource identifiers

## Schemes

| Encoding | Name | Label | Description | Type | Visibility |
| -------- | ---- | ----- | ----------- | ---- | ---------- |
| 0 | Local Untyped Domains| local | This scheme is for use with untyped local identitfiers | untyped | local |
| 1 | Local Hash Domains | hash-local | This scheme is for use with untyped local hashes | untyped | local |
| 2 | Local Type Identifier Domains|type-local|This scheme is for local process specific type identifiers|type|local|
| 3 |Local Typed Domains|typed-local|This scheme is for use with locally identitfied typed entities|typed|local|
| 4 | Local Signature Domains|sig-local|This scheme is for use with locally schemed signing|signature|local|
| 5|Local Sequence Domains|seq-local|This scheme is for use with locally schemed sequence domains|sequence|local|
| 6|Decentralized Identifier Methods|did|This scheme used for CRI's which contain Did's as defined in https://w3c-ccg.github.io/did-spec/|untyped|public|
| 7|Locally Scoped Decentralized Identifier Methods|did|This scheme used for locally defined Dids which can be used for develpment and testing and are not expected to go beyond a local limited scope|untyped|local|
| 8|JSON LD Context Domains|jsonld|This scheme is for domains which map to json ld contexts|type|public|
|9|ISO Identifier Domains|ISO|This scheme is for domains represent ISO identifiers|typed|public|
|A|Protobuf Type Url Domains|proto|This scheme is for domains which map to type urls used in protobuf any marshalling|type|public|
|B|Origanisation Identifiers|orgid|This scheme is for domain that represent organisations, such as companies, associations charities|typed|public|
| C|Private Durable Untyped Domains|untyped-private|This scheme is for scoped durable identifiers backed by a private persistance mechanism|untyped|private|
| D|Private Durable Typed Domains|typed-private|This scheme is for scoped durable identifiers backed by a private persistance mechanism|typed|private|
| E| Private Durable Sequence Domains|seq-private|This scheme is for use with durable sequence domain backed by a private persistance mechanism|sequence|local|
| F|Public unique identifier domains|uid-public|This scheme is for use with globally unique identifiers|untyped|public|
| G|Public API Key identifier|api-key|This scheme is used for access to service|apikey|public|
